INTRODUCTION
------------

Current Maintainer: Johannes Schmidt <mail@johannez.com>

Title Case uses the javascript function from David Gouch
(https://github.com/gouch/to-title-case) to convert selected strings
to title case.
Besides defining what JQuery selector to use for the conversion you
can also set exceptions for special word cases.


INSTALLATION
------------

1.  Download the to-title-case Javascript library
    (https://github.com/gouch/to-title-case) and extract the file
    under sites/all/libraries.

2.  Download and enable the module.

3.  Configure at Administer > Configuration > User interface > Title Case
    (requires administer site configuration permission)
